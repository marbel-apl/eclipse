package org.eclipse.marbel.lexer;

import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.eclipse.core.runtime.IPath;
import org.eclipse.dltk.ui.text.AbstractScriptScanner;
import org.eclipse.dltk.ui.text.IColorManager;

public interface ISourceLexer {
	Set<String> getApplicableExtensions();

	void createLexer(CharStream input);

	Lexer getLexer();

	int getWhitespaceToken();

	String getColor(int token);

	Set<Integer> getSubTokens();

	AbstractScriptScanner getSubScanner(IPath path, IColorManager manager);
}
