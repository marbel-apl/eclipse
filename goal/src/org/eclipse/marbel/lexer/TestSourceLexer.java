package org.eclipse.marbel.lexer;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.eclipse.core.runtime.IPath;
import org.eclipse.dltk.ui.text.AbstractScriptScanner;
import org.eclipse.dltk.ui.text.IColorManager;
import org.eclipse.marbel.Messages;
import org.eclipse.marbel.editor.IGoalColorConstants;

import com.google.common.collect.Maps;

import ar.com.tadp.prolog.core.PrologCorePlugin;
import ar.com.tadp.prolog.core.ui.editor.text.PrologCodeScanner;
import marbel.krFactory.KRFactory;
import marbel.languageTools.parser.TEST2GLexer;

public class TestSourceLexer implements ISourceLexer {
	private TEST2GLexer lexer;
	private final static Map<String, Class<? extends AbstractScriptScanner>> sub = Maps.newHashMapWithExpectedSize(1);

	static {
		sub.put(KRFactory.SWI_PROLOG, PrologCodeScanner.class);
	}

	@Override
	public Set<String> getApplicableExtensions() {
		final Set<String> extensions = new HashSet<>(1);
		extensions.add(Messages.TestFileExtension);
		return extensions;
	}

	@Override
	public void createLexer(final CharStream input) {
		this.lexer = new TEST2GLexer(input);
		this.lexer.removeErrorListeners();
	}

	@Override
	public Lexer getLexer() {
		return this.lexer;
	}

	@Override
	public int getWhitespaceToken() {
		return TEST2GLexer.WS;
	}

	@Override
	public String getColor(final int token) {
		switch (token) {
		case TEST2GLexer.LINE_COMMENT:
		case TEST2GLexer.BLOCK_COMMENT:
			return IGoalColorConstants.GOAL_COMMENT;
		case TEST2GLexer.DO:
		case TEST2GLexer.IF:
		case TEST2GLexer.TEST:
		case TEST2GLexer.TIMEOUT:
		case TEST2GLexer.USE:
			return IGoalColorConstants.GOAL_DECLARATION;
		case TEST2GLexer.ALWAYS:
		case TEST2GLexer.EVENTUALLY:
		case TEST2GLexer.NEVER:
		case TEST2GLexer.UNTIL:
			return IGoalColorConstants.GOAL_KEYWORD;
		case TEST2GLexer.CLBR:
		case TEST2GLexer.COMMA:
		case TEST2GLexer.CRBR:
		case TEST2GLexer.DOT:
		case TEST2GLexer.EQUALS:
		case TEST2GLexer.PLUS:
			return IGoalColorConstants.GOAL_OPERATOR;
		case TEST2GLexer.SingleQuotedStringLiteral:
		case TEST2GLexer.StringLiteral:
			return IGoalColorConstants.GOAL_STRING;
		default:
			return IGoalColorConstants.GOAL_DEFAULT;
		}
	}

	@Override
	public Set<Integer> getSubTokens() {
		final Set<Integer> subtokens = new HashSet<>(3);
		subtokens.add(TEST2GLexer.KR_IF);
		subtokens.add(TEST2GLexer.KR_THEN);
		subtokens.add(TEST2GLexer.PARLIST);
		return subtokens;
	}

	@Override
	public AbstractScriptScanner getSubScanner(final IPath path, final IColorManager manager) {
		try { // FIXME
				// final String name = GoalSourceParser.getKR(path).getName();
				// final Class<? extends AbstractScriptScanner> kr =
				// sub.get(name);
				// if (kr.equals(PrologCodeScanner.class)) {
			return new PrologCodeScanner(manager, PrologCorePlugin.getDefault().getPreferenceStore());
			// } else {
			// return null;
			// }
		} catch (final Exception e) {
			return null;
		}
	}
}