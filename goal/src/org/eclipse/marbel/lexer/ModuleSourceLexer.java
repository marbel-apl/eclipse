package org.eclipse.marbel.lexer;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.eclipse.core.runtime.IPath;
import org.eclipse.dltk.ui.text.AbstractScriptScanner;
import org.eclipse.dltk.ui.text.IColorManager;
import org.eclipse.marbel.Messages;
import org.eclipse.marbel.editor.IGoalColorConstants;

import com.google.common.collect.Maps;

import ar.com.tadp.prolog.core.PrologCorePlugin;
import ar.com.tadp.prolog.core.ui.editor.text.PrologCodeScanner;
import marbel.krFactory.KRFactory;
import marbel.languageTools.parser.MOD2GLexer;

public class ModuleSourceLexer implements ISourceLexer {
	private MOD2GLexer lexer;
	private final static Map<String, Class<? extends AbstractScriptScanner>> sub = Maps.newHashMapWithExpectedSize(1);

	static {
		sub.put(KRFactory.SWI_PROLOG, PrologCodeScanner.class);
	}

	@Override
	public Set<String> getApplicableExtensions() {
		final Set<String> extensions = new HashSet<>(1);
		extensions.add(Messages.ModuleFileExtension);
		return extensions;
	}

	@Override
	public void createLexer(final CharStream input) {
		this.lexer = new MOD2GLexer(input);
		this.lexer.removeErrorListeners();
	}

	@Override
	public Lexer getLexer() {
		return this.lexer;
	}

	@Override
	public int getWhitespaceToken() {
		return MOD2GLexer.WS;
	}

	@Override
	public String getColor(final int token) {
		switch (token) {
		case MOD2GLexer.LINE_COMMENT:
		case MOD2GLexer.BLOCK_COMMENT:
			return IGoalColorConstants.GOAL_COMMENT;
		case MOD2GLexer.DELETE:
		case MOD2GLexer.EXIT:
		case MOD2GLexer.EXITMODULE:
		case MOD2GLexer.FORALL:
		case MOD2GLexer.IF:
		case MOD2GLexer.INSERT:
		case MOD2GLexer.LOG:
		case MOD2GLexer.MODULE:
		case MOD2GLexer.ORDER:
		case MOD2GLexer.PRINT:
		case MOD2GLexer.SEND:
		case MOD2GLexer.SLEEP:
		case MOD2GLexer.STARTTIMER:
		case MOD2GLexer.SUBSCRIBE:
		case MOD2GLexer.UNSUBSCRIBE:
		case MOD2GLexer.USE:
			return IGoalColorConstants.GOAL_DECLARATION;
		case MOD2GLexer.ALWAYS:
		case MOD2GLexer.NEVER:
		case MOD2GLexer.RANDOM:
		case MOD2GLexer.RANDOMALL:
		case MOD2GLexer.LINEAR:
		case MOD2GLexer.LINEARRANDOM:
		case MOD2GLexer.LINEARALL:
		case MOD2GLexer.LINEARALLRANDOM:
		case MOD2GLexer.NOACTION:
			return IGoalColorConstants.GOAL_KEYWORD;
		case MOD2GLexer.CLBR:
		case MOD2GLexer.COMMA:
		case MOD2GLexer.CRBR:
		case MOD2GLexer.DOT:
		case MOD2GLexer.EQUALS:
		case MOD2GLexer.PLUS:
			return IGoalColorConstants.GOAL_OPERATOR;
		case MOD2GLexer.SingleQuotedStringLiteral:
		case MOD2GLexer.StringLiteral:
			return IGoalColorConstants.GOAL_STRING;
		default:
			return IGoalColorConstants.GOAL_DEFAULT;
		}
	}

	@Override
	public Set<Integer> getSubTokens() {
		final Set<Integer> subtokens = new HashSet<>(3);
		subtokens.add(MOD2GLexer.KR_FORALLDO);
		subtokens.add(MOD2GLexer.KR_IFTHEN);
		subtokens.add(MOD2GLexer.PARLIST);
		return subtokens;
	}

	@Override
	public AbstractScriptScanner getSubScanner(final IPath path, final IColorManager manager) {
		try { // FIXME
				// final String name = GoalSourceParser.getKR(path).getName();
				// final Class<? extends AbstractScriptScanner> kr =
				// sub.get(name);
				// if (kr.equals(PrologCodeScanner.class)) {
			return new PrologCodeScanner(manager, PrologCorePlugin.getDefault().getPreferenceStore());
			// } else {
			// return null;
			// }
		} catch (final Exception e) {
			return null;
		}
	}
}