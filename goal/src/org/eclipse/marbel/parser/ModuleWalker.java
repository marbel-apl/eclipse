package org.eclipse.marbel.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ErrorNodeImpl;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.dltk.ast.ASTListNode;
import org.eclipse.dltk.ast.declarations.Argument;
import org.eclipse.dltk.ast.declarations.MethodDeclaration;
import org.eclipse.dltk.ast.declarations.ModuleDeclaration;
import org.eclipse.dltk.ast.declarations.TypeDeclaration;
import org.eclipse.dltk.ast.expressions.StringLiteral;
import org.eclipse.dltk.ast.references.SimpleReference;
import org.eclipse.dltk.ast.statements.Block;

import marbel.cognitiveKr.CognitiveKR;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.languageTools.InputStreamPosition;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.Validator;
import marbel.languageTools.analyzer.ValidatorSecondPass;
import marbel.languageTools.errors.module.ModuleErrorStrategy;
import marbel.languageTools.parser.MOD2GLexer;
import marbel.languageTools.parser.MOD2GParser;
import marbel.languageTools.parser.MOD2GParser.ActionContext;
import marbel.languageTools.parser.MOD2GParser.ActioncomboContext;
import marbel.languageTools.parser.MOD2GParser.BuiltinContext;
import marbel.languageTools.parser.MOD2GParser.ExitoptionContext;
import marbel.languageTools.parser.MOD2GParser.ModuleContext;
import marbel.languageTools.parser.MOD2GParser.OptionContext;
import marbel.languageTools.parser.MOD2GParser.OrderoptionContext;
import marbel.languageTools.parser.MOD2GParser.RefContext;
import marbel.languageTools.parser.MOD2GParser.RulesContext;
import marbel.languageTools.parser.MOD2GParser.StringContext;
import marbel.languageTools.parser.MOD2GParser.UseclauseContext;
import marbel.languageTools.parser.MOD2GParserVisitor;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.UserSpecOrModuleCall;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.PrologTerm;

public class ModuleWalker extends Validator<MOD2GLexer, MOD2GParser, ModuleErrorStrategy, Module>
		implements MOD2GParserVisitor<Object> {
	private MOD2GParser parser;
	private static ModuleErrorStrategy strategy = null;
	private ModuleDeclaration dltk = new ModuleDeclaration(0);

	public ModuleWalker(final String filename, final FileRegistry registry) {
		super(filename, registry);
	}

	@Override
	protected ParseTree startParser() {
		return this.parser.module();
	}

	@Override
	protected ModuleErrorStrategy getTheErrorStrategy() {
		if (strategy == null) {
			strategy = new ModuleErrorStrategy();
		}
		return strategy;
	}

	@Override
	protected ValidatorSecondPass createSecondPass() {
		return null;
	}

	@Override
	protected MOD2GLexer getNewLexer(final CharStream stream) {
		return new MOD2GLexer(stream);
	}

	@Override
	protected MOD2GParser getNewParser(final TokenStream stream) {
		this.parser = new MOD2GParser(stream);
		return this.parser;
	}

	@Override
	protected Module getNewProgram(final File file) throws IOException {
		return new Module(this.registry, new InputStreamPosition(0, 0, 0, 0, file.getCanonicalPath()));
	}

	@Override
	public Module getProgram() {
		return (Module) super.getProgram();
	}

	public ModuleDeclaration getDeclaration() {
		return this.dltk;
	}

	@Override
	public Void visit(final ParseTree tree) {
		tree.accept(this);
		return null;
	}

	// -------------------------------------------------------------
	// Module
	// -------------------------------------------------------------

	@Override
	public Void visitModule(final ModuleContext ctx) {
		final ModuleDeclaration declaration = new ModuleDeclaration(ctx.getText().length());

		// Process use clauses.
		for (final UseclauseContext useclausectx : ctx.useclause()) {
			final SimpleReference useclause = visitUseclause(useclausectx);
			if (useclause != null) {
				declaration.addStatement(useclause);
			}
		}

		if (!getProgram().resolveKRInterface()) {
			this.dltk = declaration;
			return null;
		}

		// Process options: exit, focus, order.
		for (final OptionContext optionctx : ctx.option()) {
			final SimpleReference option = visitOption(optionctx);
			if (option != null) {
				declaration.addStatement(option);
			}
		}

		// Get module name.
		final StringLiteral moduleName = (ctx.ID() == null) ? null
				: new StringLiteral(ctx.ID().getSymbol().getStartIndex(), ctx.ID().getSymbol().getStopIndex() + 1,
						ctx.ID().getText());
		final MethodDeclaration module = new MethodDeclaration(moduleName.getValue(), moduleName.start(),
				moduleName.end(), ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1);
		// Get module parameters.
		if (ctx.PARLIST() != null && // need to check whether something has gone
										// seriously wrong here
				!(ctx.PARLIST() instanceof ErrorNodeImpl)) {
			final List<Var> vars = visitVARLIST(ctx.PARLIST(), ctx);
			for (final Var var : vars) {
				final SimpleReference arg = new SimpleReference(var.getSourceInfo().getStartIndex(),
						var.getSourceInfo().getStopIndex() + 1, var.toString());
				module.addArgument(new Argument(arg, arg.start(), arg.end(), null, 0));
			}
		}

		// Process rules.
		final ASTListNode body = new ASTListNode(ctx.getStart().getStartIndex() + 1, ctx.getStop().getStopIndex());
		for (final RulesContext rulectx : ctx.rules()) {
			final TypeDeclaration rule = new TypeDeclaration("", rulectx.getStart().getStartIndex(),
					rulectx.getStop().getStopIndex() + 1, rulectx.getStart().getStartIndex(),
					rulectx.getStop().getStopIndex() + 1);
			final Block ruleBody = visitRules(rulectx);
			if (ruleBody != null) {
				rule.setBody(ruleBody);
			}
			body.addNode(rule);
		}
		module.setBody(body);

		declaration.addStatement(module);
		this.dltk = declaration;
		return null;
	}

	@Override
	public SimpleReference visitUseclause(final UseclauseContext ctx) {
		String reference = "";
		for (final RefContext ref : ctx.ref()) {
			if (!reference.isEmpty()) {
				reference += ",";
			}
			reference += visitRef(ref);
		}

		// Create use clause and resolve reference.
		final UseClause useClause = new UseClause(reference, null, null, getPathRelativeToSourceFile(""),
				getSourceInfo(ctx));
		useClause.getOrResolveReferences();
		getProgram().addUseClause(useClause);

		return new SimpleReference(ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1, reference);
	}

	@Override
	public String visitRef(final RefContext ctx) {
		if (ctx == null) {
			return "";
		} else if (ctx.string() != null) {
			return visitString(ctx.string());
		} else {
			String path = "";
			for (final String component : ctx.getText().split("\\.")) {
				path = FilenameUtils.concat(path, component);
			}
			return path;
		}
	}

	@Override
	public SimpleReference visitOption(final OptionContext ctx) {
		String option = "";
		if (ctx.exitoption() != null) {
			option = visitExitoption(ctx.exitoption());
		} else if (ctx.orderoption() != null) {
			option = visitOrderoption(ctx.orderoption());
		}
		return new SimpleReference(ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1, option);
	}

	@Override
	public String visitExitoption(final ExitoptionContext ctx) {
		return ctx.EXIT().getText() + "=" + (ctx.value == null ? "" : ctx.value.getText());
	}

	@Override
	public String visitOrderoption(final OrderoptionContext ctx) {
		return ctx.ORDER().getText() + "=" + (ctx.value == null ? "" : ctx.value.getText());
	}

	@Override
	public Block visitRules(final RulesContext ctx) {
		final Block body = new Block(ctx.getStart().getStartIndex() + 1, ctx.getStop().getStopIndex());

		// Get mental state condition.
		/*
		 * if (ctx.msc() != null) { final List<TypeDeclaration> conditions =
		 * visitMsc(ctx.msc()); for (final TypeDeclaration condition : conditions) {
		 * body.addStatement(condition); } }
		 */

		// Get action combo.
		if (ctx.actioncombo() != null) {
			final List<TypeDeclaration> combo = visitActioncombo(ctx.actioncombo());
			for (final TypeDeclaration action : combo) {
				body.addStatement(action);
			}
		}

		// Get nested rules.
		if (ctx.rules() != null) {
			for (final RulesContext rulectx : ctx.rules()) {
				final Block nested = visitRules(rulectx);
				if (nested != null) {
					body.addStatement(nested);
				}
			}
		}

		return body;
	}

	@Override
	public List<TypeDeclaration> visitActioncombo(final ActioncomboContext ctx) {
		final List<TypeDeclaration> combo = new ArrayList<>();
		for (final ActionContext actionCtx : ctx.action()) {
			final TypeDeclaration action = visitAction(actionCtx);
			if (action != null) {
				combo.add(action);
			}
		}
		return combo;
	}

	@Override
	public TypeDeclaration visitAction(final ActionContext ctx) {
		if (ctx.ID() != null && ctx.PARLIST() != null) {
			final List<Term> parameters = visitPARLIST(ctx.PARLIST(), getSourceInfo(ctx.PARLIST()));
			final UserSpecOrModuleCall call = new UserSpecOrModuleCall(ctx.ID().getText(), parameters,
					getSourceInfo(ctx));
			return new TypeDeclaration(call.toString(), ctx.getStart().getStartIndex(),
					ctx.getStop().getStopIndex() + 1, ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1);
		} else if (ctx.builtin() != null) {
			return visitBuiltin(ctx.builtin());
		} else {
			return null;
		}
	}

	@Override
	public TypeDeclaration visitBuiltin(final BuiltinContext ctx) {
		// Construct action.
		final String op = (ctx.op == null) ? "" : ctx.op.getText();
		if (ctx.PARLIST() != null) {
			final TypeDeclaration action = new TypeDeclaration(op, ctx.getStart().getStartIndex(),
					ctx.getStart().getStartIndex() + op.length(), ctx.PARLIST().getSymbol().getStartIndex(),
					ctx.PARLIST().getSymbol().getStopIndex() + 1);
			final Block body = new Block(ctx.PARLIST().getSymbol().getStartIndex() + 1,
					ctx.PARLIST().getSymbol().getStopIndex());

			// Get parameters.
			List<Term> parameters = null;
			final String krFragment = removeLeadTrailCharacters(ctx.PARLIST().getText());
			if (!krFragment.isEmpty()) {
				try {
					final CognitiveKR ckr = getCognitiveKR();
					parameters = ckr.visitArguments(krFragment, getSourceInfo(ctx.PARLIST()));
				} catch (final ParserException e) {
					// TODO
				}
			}

			if (parameters != null) {
				final List<marbel.krInterface.language.Expression> base = new ArrayList<>(parameters.size());
				base.addAll(parameters);
				addBaseToBody(ctx.PARLIST().getSymbol(), body, base);
			}

			action.setBody(body);
			return action;
		} else {
			return new TypeDeclaration(op, ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1,
					ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex() + 1);
		}
	}

	@Override
	public String visitString(final StringContext ctx) {
		final StringBuilder str = new StringBuilder();
		if (ctx.StringLiteral() != null) {
			for (final TerminalNode literal : ctx.StringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)\"", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\\"", "\""));
				}
			}
		}
		if (ctx.SingleQuotedStringLiteral() != null) {
			for (final TerminalNode literal : ctx.SingleQuotedStringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)'", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\'", "'"));
				}
			}
		}
		return str.toString();
	}

	// -------------------------------------------------------------
	// Helpers
	// -------------------------------------------------------------

	private static List<String> fromGoalExpression(final marbel.krInterface.language.Expression expression) {
		final List<String> result = new ArrayList<>();
		if (expression instanceof PrologTerm) {
			processIndidvidualTerms(result, (PrologTerm) expression);
		}
		return result;
	}

	private static void processIndidvidualTerms(final List<String> result, final PrologTerm term) {
		if (term instanceof PrologCompound) {
			final PrologCompound compound = (PrologCompound) term;
			result.add(compound.getName());
			for (final Term arg : compound) {
				processIndidvidualTerms(result, (PrologTerm) arg);
			}
		} else {
			result.add(term.toString());
		}
	}

	private static void addBaseToBody(final Token start, final Block body,
			final List<marbel.krInterface.language.Expression> base) {
		int pos = start.getStartIndex();
		final Pattern alphanum = Pattern.compile("[a-zA-Z0-9]");
		for (final marbel.krInterface.language.Expression exp : base) {
			for (final String term : fromGoalExpression(exp)) {
				if (alphanum.matcher(term).find()) { // no symbols
					final TypeDeclaration declaration = new TypeDeclaration(term, pos, pos + term.length(), pos,
							pos + term.length());
					body.addStatement(declaration);
				}
				pos += term.length();
			}
		}
	}
}
