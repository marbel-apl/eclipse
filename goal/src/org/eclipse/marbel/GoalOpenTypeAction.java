package org.eclipse.marbel;

import org.eclipse.dltk.ui.IDLTKUILanguageToolkit;
import org.eclipse.dltk.ui.actions.OpenTypeAction;
import org.eclipse.marbel.ui.GoalUILanguageToolkit;

public class GoalOpenTypeAction extends OpenTypeAction {
	@Override
	protected IDLTKUILanguageToolkit getUILanguageToolkit() {
		return new GoalUILanguageToolkit();
	}
}