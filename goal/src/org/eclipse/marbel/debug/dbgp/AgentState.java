package org.eclipse.marbel.debug.dbgp;

import java.io.File;
import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

import marbel.languageTools.program.agent.AgentId;
import marbel.tools.history.EventStorage;

public class AgentState {
	private boolean initialized = false;
	private final AgentId agent;
	private final Set<String> beliefs;
	private final Deque<String> modules;
	private Set<String> condition;
	private EventStorage history;
	private String runmode;

	public AgentState(final AgentId agent) {
		this.agent = agent;
		this.beliefs = new LinkedHashSet<>();
		this.modules = new LinkedList<>();
		this.condition = new LinkedHashSet<>();
		this.runmode = "initializing";
	}

	public void reset() {
		this.beliefs.clear();
		this.modules.clear();
		this.condition.clear();
	}

	public EventStorage getHistory(final LocalDebugger debugger) {
		if (this.history == null) {
			try {
				final File datafile = new File(debugger.getHistoryState(this.agent));
				this.history = new EventStorage(datafile);
			} catch (final Exception ignore) {
				this.history = null;
			}
		}
		return this.history;
	}

	public void setRunMode(final String mode) {
		this.initialized = true;
		this.runmode = mode;
	}

	public void deinitialize() {
		this.initialized = false;
	}

	public void addBelief(final String belief) {
		this.beliefs.add(belief);
	}

	public void removeBelief(final String belief) {
		this.beliefs.remove(belief);
	}

	public void setModule(final String module) {
		if (this.modules.isEmpty() || !module.equals(this.modules.peek())) {
			this.modules.push(module);
		}
	}

	public void removeModule() {
		if (!this.modules.isEmpty()) {
			this.modules.pop();
		}
	}

	public void setCondition(final Set<String> condition) {
		this.condition = condition;
	}

	public boolean isInitialized() {
		return this.initialized;
	}

	public String getRunMode() {
		return this.runmode;
	}

	public Set<String> getBeliefs() {
		return this.beliefs;
	}

	public String getModule() {
		if (this.modules.isEmpty()) {
			return "";
		} else {
			return this.modules.peek();
		}
	}

	public Set<String> getCondition() {
		return this.condition;
	}
}
