package org.eclipse.marbel.debug;

public final class GoalDebugConstants {
	public static final String DEBUG_MODEL_ID = "org.eclipse.dltk.debug.marbelModel";
	public static final String DEBUGGING_ENGINE_ID_KEY = "debugging_engine_id";

	private GoalDebugConstants() {
	}
}