package org.eclipse.marbel.debug.ui;

import org.eclipse.dltk.debug.ui.AbstractDebugUILanguageToolkit;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.marbel.Activator;
import org.eclipse.marbel.debug.GoalDebugConstants;

public class GoalDebugUILanguageToolkit extends AbstractDebugUILanguageToolkit {
	@Override
	public String getDebugModelId() {
		return GoalDebugConstants.DEBUG_MODEL_ID;
	}

	@Override
	public IPreferenceStore getPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}
}
