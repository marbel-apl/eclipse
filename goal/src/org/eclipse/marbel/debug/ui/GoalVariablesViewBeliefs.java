package org.eclipse.marbel.debug.ui;

public class GoalVariablesViewBeliefs extends GoalVariablesView {
	public final static String VIEW_ID = "org.eclipse.marbel.debug.ui.GoalVariablesViewBeliefs";

	@Override
	protected GoalVariableType getVariableType() {
		return GoalVariableType.BELIEFS;
	}
}