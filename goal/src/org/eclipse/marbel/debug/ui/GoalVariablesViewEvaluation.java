package org.eclipse.marbel.debug.ui;

public class GoalVariablesViewEvaluation extends GoalVariablesView {
	public final static String VIEW_ID = "org.eclipse.marbel.debug.ui.GoalVariablesViewEvaluation";

	@Override
	protected GoalVariableType getVariableType() {
		return GoalVariableType.EVALUATION;
	}
}