package org.eclipse.marbel.launch;

import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.dltk.launching.AbstractInterpreterInstall;
import org.eclipse.dltk.launching.IInterpreterInstallType;
import org.eclipse.dltk.launching.IInterpreterRunner;
import org.eclipse.marbel.GoalNature;

public class GoalInterpreterInstall extends AbstractInterpreterInstall {
	public GoalInterpreterInstall(final IInterpreterInstallType type, final String id) {
		super(type, id);
	}

	@Override
	public IInterpreterRunner getInterpreterRunner(final String mode) {
		final IInterpreterRunner runner = super.getInterpreterRunner(mode);
		if (runner != null) {
			return runner;
		} else if (mode.equals(ILaunchManager.RUN_MODE)) {
			return new GoalInterpreterRunner(this);
		} else {
			return null;
		}
	}

	@Override
	public String getNatureId() {
		return GoalNature.GOAL_NATURE;
	}
}
