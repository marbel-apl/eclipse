package org.eclipse.marbel.launch;

import org.eclipse.debug.core.ILaunch;
import org.eclipse.dltk.launching.IInterpreterInstall;
import org.eclipse.dltk.launching.InterpreterConfig;
import org.eclipse.marbel.launching.AbstractRunnableInterpreterRunner;
import org.eclipse.marbel.launching.RunnableProcess;

public class GoalInterpreterRunner extends AbstractRunnableInterpreterRunner {
	public GoalInterpreterRunner(final IInterpreterInstall install) {
		super(install);
	}

	@Override
	protected RunnableProcess createRunnableProcess(final ILaunch launch, final InterpreterConfig config) {
		return new GoalRunnableProcess(getInstall(), launch, config);
	}
}