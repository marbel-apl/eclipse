package org.eclipse.marbel.launch;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.dltk.debug.ui.launchConfigurations.ScriptCommonTab;

public class GoalLaunchConfigurationTabGroup extends AbstractLaunchConfigurationTabGroup {
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final ILaunchConfigurationTab[] tabs = { new GoalLaunchConfigurationTab(mode),
				new ScriptCommonTab(), /* new GoalLaunchConfigurationLogging() */ };
		setTabs(tabs);
	}
}
