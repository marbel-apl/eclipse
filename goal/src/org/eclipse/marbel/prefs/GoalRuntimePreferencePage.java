package org.eclipse.marbel.prefs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditor;

import marbel.preferences.CorePreferences;
import marbel.preferences.LoggingPreferences;
import marbel.preferences.ProfilerPreferences;
import marbel.tools.profiler.InfoType;

public class GoalRuntimePreferencePage extends GoalPreferencePage {
	@Override
	protected void createFieldEditors() {
		final GroupFieldEditor general = new GroupFieldEditor("General", getFieldEditorParent());
		final List<FieldEditor> generalFields = new ArrayList<>(2);
		generalFields.add(new BooleanFieldEditor(CorePreferences.Pref.agentCopyEnvState.name(),
				"New agents copy the environment's state", general.getFieldEditorParent()));
		generalFields.add(new BooleanFieldEditor(CorePreferences.Pref.printEntities.name(),
				"Print a message when an entity (dis)appears", general.getFieldEditorParent()));
		general.setFieldEditors(generalFields);
		addField(general);

		final GroupFieldEditor performance = new GroupFieldEditor("Performance", getFieldEditorParent());
		final List<FieldEditor> performanceFields = new ArrayList<>(3);
		performanceFields.add(new BooleanFieldEditor(CorePreferences.Pref.sleepRepetitiveAgent.name(),
				"Sleep agents when they get no new messages or percepts", performance.getFieldEditorParent()));
		performanceFields.add(new BooleanFieldEditor(CorePreferences.Pref.removeKilledAgent.name(),
				"Remove agents completely when they are killed", performance.getFieldEditorParent()));
		performanceFields.add(new BooleanFieldEditor(CorePreferences.Pref.sequentialExecution.name(),
				"Execute multiple agents in sequence (instead of in parallel)", performance.getFieldEditorParent()));
		performance.setFieldEditors(performanceFields);
		addField(performance);

		final GroupFieldEditor debugging = new GroupFieldEditor("Debugging and testing", getFieldEditorParent());
		final List<FieldEditor> debuggingFields = new ArrayList<>(3);
		debuggingFields.add(new BooleanFieldEditor(CorePreferences.Pref.globalBreakpoints.name(),
				"Pause all agents upon hitting a user breakpoint", debugging.getFieldEditorParent()));
		debuggingFields.add(new BooleanFieldEditor(CorePreferences.Pref.abortOnTestFailure.name(),
				"Abort tests directly upon first failure", debugging.getFieldEditorParent()));
		debuggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.enableHistory.name(),
				"Enable trace recording (history)", debugging.getFieldEditorParent()));
		debugging.setFieldEditors(debuggingFields);
		addField(debugging);

		final GroupFieldEditor profiling = new GroupFieldEditor("Profiling", getFieldEditorParent());
		final InfoType[] types = InfoType.values();
		final List<FieldEditor> profilingFields = new ArrayList<>(3 + types.length);
		profilingFields.add(new BooleanFieldEditor(ProfilerPreferences.Pref.profiling.name(), "Enable profiling",
				profiling.getFieldEditorParent()));
		profilingFields.add(new BooleanFieldEditor(ProfilerPreferences.Pref.profilingToFile.name(),
				"Save profiling data to file(s)", profiling.getFieldEditorParent()));
		profilingFields.add(new BooleanFieldEditor(ProfilerPreferences.Pref.logNodeID.name(),
				"Include profile node IDs", profiling.getFieldEditorParent()));
		for (final InfoType type : types) {
			profilingFields.add(new BooleanFieldEditor(type.name(), "Profile " + type.getDescription(),
					profiling.getFieldEditorParent()));
		}
		profiling.setFieldEditors(profilingFields);
		addField(profiling);
	}
}