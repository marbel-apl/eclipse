package org.eclipse.marbel.prefs;

import org.eclipse.dltk.ui.templates.ScriptTemplateAccess;
import org.eclipse.dltk.ui.templates.ScriptTemplatePreferencePage;
import org.eclipse.dltk.ui.text.ScriptSourceViewerConfiguration;
import org.eclipse.jface.text.IDocument;
import org.eclipse.marbel.Activator;
import org.eclipse.marbel.completion.GoalTemplateAccess;
import org.eclipse.marbel.editor.GoalSourceViewerConfiguration;
import org.eclipse.marbel.editor.GoalTextTools;
import org.eclipse.marbel.editor.IGoalPartitions;

public class GoalTemplatePreferencePage extends ScriptTemplatePreferencePage {
	@Override
	protected ScriptSourceViewerConfiguration createSourceViewerConfiguration() {
		return new GoalSourceViewerConfiguration(getTextTools().getColorManager(), getPreferenceStore(), null,
				IGoalPartitions.GOAL_PARTITIONING);
	}

	@Override
	protected void setDocumentParticioner(final IDocument document) {
		getTextTools().setupDocumentPartitioner(document, IGoalPartitions.GOAL_PARTITIONING);
	}

	@Override
	protected void setPreferenceStore() {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected ScriptTemplateAccess getTemplateAccess() {
		return GoalTemplateAccess.getInstance();
	}

	private GoalTextTools getTextTools() {
		return Activator.getDefault().getTextTools();
	}
}