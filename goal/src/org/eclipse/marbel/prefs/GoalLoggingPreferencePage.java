package org.eclipse.marbel.prefs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;

import marbel.preferences.LoggingPreferences;
import marbel.tools.debugger.events.DebugChannel;

public class GoalLoggingPreferencePage extends GoalPreferencePage {
	@Override
	protected void createFieldEditors() {
		final GroupFieldEditor logging = new GroupFieldEditor("Log Handling", getFieldEditorParent());
		final List<FieldEditor> loggingFields = new ArrayList<>(7);
		loggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.showlogtime.name(), "Show log time",
				logging.getFieldEditorParent()));
		loggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.logtofile.name(), "Write logs to files",
				logging.getFieldEditorParent()));
		loggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.overwritelogfiles.name(),
				"Overwrite old log files", logging.getFieldEditorParent()));
		loggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.logconsoles.name(),
				"Log the default consoles to file", logging.getFieldEditorParent()));
		loggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.eclipseActionHistory.name(),
				"Show an action history whilst debugging", logging.getFieldEditorParent()));
		loggingFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.eclipseAgentConsoles.name(),
				"Show a console per agent whilst debugging", logging.getFieldEditorParent()));
		loggingFields.add(new FileFieldEditor(LoggingPreferences.Pref.logdirectory.name(), "Logging directory:",
				logging.getFieldEditorParent()));
		logging.setFieldEditors(loggingFields);
		addField(logging);

		final GroupFieldEditor log = new GroupFieldEditor("Logging options", getFieldEditorParent());
		final List<FieldEditor> logFields = new ArrayList<>(13);
		logFields.add(
				new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.REASONING_CYCLE_SEPARATOR.name(),
						DebugChannel.REASONING_CYCLE_SEPARATOR.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.printStats.name(),
				"Include statistics each cycle separator", log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.SLEEP.name(),
				DebugChannel.SLEEP.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.CALL_ACTION_OR_MODULE.name(),
				DebugChannel.CALL_ACTION_OR_MODULE.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.MODULE_ENTRY.name(),
				DebugChannel.MODULE_ENTRY.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.MODULE_EXIT.name(),
				DebugChannel.MODULE_EXIT.getExplanation(), log.getFieldEditorParent()));
		logFields.add(
				new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.RULE_CONDITION_EVALUATION.name(),
						DebugChannel.RULE_CONDITION_EVALUATION.getExplanation(), log.getFieldEditorParent()));
		logFields
				.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.ACTION_EXECUTED_BUILTIN.name(),
						DebugChannel.ACTION_EXECUTED_BUILTIN.getExplanation(), log.getFieldEditorParent()));
		logFields.add(
				new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.ACTION_EXECUTED_MESSAGING.name(),
						DebugChannel.ACTION_EXECUTED_MESSAGING.getExplanation(), log.getFieldEditorParent()));
		logFields.add(
				new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.ACTION_EXECUTED_USERSPEC.name(),
						DebugChannel.ACTION_EXECUTED_USERSPEC.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.KR_UPDATES.name(),
				DebugChannel.KR_UPDATES.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.MAILS.name(),
				DebugChannel.MAILS.getExplanation(), log.getFieldEditorParent()));
		logFields.add(new BooleanFieldEditor(GoalPreferenceInitializer.LOG + DebugChannel.PERCEPTS.name(),
				DebugChannel.PERCEPTS.getExplanation(), log.getFieldEditorParent()));
		log.setFieldEditors(logFields);
		addField(log);

		final GroupFieldEditor warnings = new GroupFieldEditor("Warnings", getFieldEditorParent());
		final List<FieldEditor> warningFields = new ArrayList<>(2);
		warningFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.stackdump.name(),
				"Show stack traces with warnings", warnings.getFieldEditorParent()));
		warningFields.add(new BooleanFieldEditor(LoggingPreferences.Pref.eclipseDebug.name(),
				"Put Eclipse in verbose debug mode", warnings.getFieldEditorParent()));
		warnings.setFieldEditors(warningFields);
		addField(warnings);
	}
}