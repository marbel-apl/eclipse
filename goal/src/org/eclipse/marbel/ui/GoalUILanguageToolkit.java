package org.eclipse.marbel.ui;

import org.eclipse.dltk.core.IDLTKLanguageToolkit;
import org.eclipse.dltk.ui.AbstractDLTKUILanguageToolkit;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.marbel.Activator;
import org.eclipse.marbel.GoalLanguageToolkit;

public class GoalUILanguageToolkit extends AbstractDLTKUILanguageToolkit {
	@Override
	public IPreferenceStore getPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}

	@Override
	public IDLTKLanguageToolkit getCoreToolkit() {
		return GoalLanguageToolkit.getDefault();
	}
}