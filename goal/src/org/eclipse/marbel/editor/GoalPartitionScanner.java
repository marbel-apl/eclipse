package org.eclipse.marbel.editor;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;

public class GoalPartitionScanner extends RuleBasedPartitionScanner {
	public GoalPartitionScanner() {
		final IPredicateRule[] rules = new IPredicateRule[2];

		final IToken comment = new Token(IGoalPartitions.GOAL_COMMENT);
		rules[0] = new EndOfLineRule("%", comment);
		rules[1] = new MultiLineRule("/*", "*/", comment);

		setPredicateRules(rules);
	}
}