package org.eclipse.marbel.editor;

import org.eclipse.jface.text.IDocument;

public interface IGoalPartitions {
	String GOAL_PARTITIONING = "__marbel_partitioning";
	String GOAL_COMMENT = "__marbel_comment";
	String[] GOAL_PARITION_TYPES = { GOAL_COMMENT, IDocument.DEFAULT_CONTENT_TYPE };
}