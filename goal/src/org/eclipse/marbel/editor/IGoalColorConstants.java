package org.eclipse.marbel.editor;

import org.eclipse.dltk.ui.text.DLTKColorConstants;

public interface IGoalColorConstants {
	String GOAL_COMMENT = DLTKColorConstants.DLTK_SINGLE_LINE_COMMENT;
	String GOAL_KEYWORD = DLTKColorConstants.DLTK_KEYWORD;
	String GOAL_STRING = DLTKColorConstants.DLTK_STRING;
	String GOAL_DEFAULT = DLTKColorConstants.DLTK_DEFAULT;
	String GOAL_OPERATOR = "MARBEL_operator";
	String GOAL_DECLARATION = "MARBEL_declaration";

	String EDITOR_MATCHING_BRACKETS = "matchingBrackets";
	String EDITOR_MATCHING_BRACKETS_COLOR = "matchingBracketsColor";
}